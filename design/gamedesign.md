# Was, warum? 
### PITCH!
Spieler mittleren Alters spielt Entscheidungsträger für eine in der Galaxy colonisierend aufstrebende Zivilisation welche durch einer KI simuliert wird.
Dem Spieler werden anhand seiner Taten die dadurch resultierenden Auswirkungen wieder gespiegelt.
In kurz, ernte was du säest.
Dem Spieler soll dadurch das natürliche und unnatürliche/künstliche, anhand der Simulationen der KI, dessen Auswirkung ersichtlich werden.

### Details
- Das Wesen, dem Spieler seine Auswirkungen/Resultate/(Ver)Antworten auf seine Taten/Entscheidungen/Handlungen zu reflektieren. 
- In Form eines 
- Galaxy entdecken, neu colonisieren, gestalten, aufbruch in das neue
- Es gibt nur ein ein Gesetz, das Gesetz der Natürlichkeit. Der Spieler kann mit seinen Handel es nutzen oder degegen verstoßen.
- Gesellschaftsformen beeinflussen den Verlauf/die Scheere zwischen dem natrürlichen und unnatürlichen/künstlichen
- Selbstregulierende System welche durch den Spieler beeinlussbar sind
- 3-15min pro session
- Einfach zu begreifen, schwer zu meistern

# Genre
- Sifi
- Aufbau
- Strategie 
- Simulation

# Zielgruppe?
- Gelegenheitsspieler
- 30-60 Jahre
- Jugendfrei FSK0

# Wo?
- Internet Browser
- mobile

# Wie spiele ich es? (Ablauf einer Spielszene)
### Anfang
Nachdem der Spieler sich eingeloggt hat, findet er sich auf dem Kolonieschiff im Heimatplanet Orbit wieder.
Er hat nun zu entscheiden wie seine zukunftige Zivilisation ihre neue Heimat beleben möchte. Dafür wählt er zwischen den bekannten Staats/Lebensformen.

* KI: künstlich
* Na: natürlich
* t verfall
* ↑ lebendige


![alt text](ausrichtung_zivilistaion_verhalten.jpg "Zivilisationsausrichtung")
